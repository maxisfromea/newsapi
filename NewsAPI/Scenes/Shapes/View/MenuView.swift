//
//  MenuView.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 26/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import UIKit

protocol MenuViewDelegate: class {
    func menuDidSelect(shape: MenuView.Shape)
}

class MenuView: UIView {
    enum Shape: Int {
        case circle
        case triangle
        case star
    }

    weak var delegate: MenuViewDelegate?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        layer.cornerRadius = bounds.width/2
        layer.masksToBounds = true
        translatesAutoresizingMaskIntoConstraints = false

        heightAnchor.constraint(equalToConstant: 120).isActive = true
    }

    @IBAction func circleSelected(sender: Any) {
        delegate?.menuDidSelect(shape: .circle)
    }

    @IBAction func starSelected(sender: Any) {
        delegate?.menuDidSelect(shape: .star)
    }

    @IBAction func triangleSelected(sender: Any) {
        delegate?.menuDidSelect(shape: .triangle)
    }
}
