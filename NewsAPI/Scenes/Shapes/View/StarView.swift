//
//  StarView.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 26/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import UIKit

@IBDesignable
class StarView: UIView {

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.setFillColor(UIColor.blue.cgColor)

        let xCenter = rect.midX;
        let yCenter = rect.midY;

        let width: CGFloat = min(rect.width, rect.height) - 10;
        let radius: CGFloat = width / 2.0;
        let flip: CGFloat = -1.0;
        let angle = CGFloat(2.0 * Double.pi * (2.0 / 5.0))

        context.move(to: CGPoint(x: CGFloat(xCenter), y: CGFloat(radius * flip + yCenter)))

        for index in 1...4 {
            let x = radius * sin(CGFloat(index) * angle);
            let y = radius * cos(CGFloat(index) * angle);
            context.addLine(to: CGPoint(x: CGFloat(x + xCenter), y: CGFloat(y * flip + yCenter)))
        }

        context.closePath();
        context.fillPath();
    }
}
