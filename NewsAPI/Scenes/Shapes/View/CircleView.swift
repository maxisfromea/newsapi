//
//  CircleView.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 26/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import UIKit

@IBDesignable
class CircleView: UIView {

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }

        let side = min(rect.width, rect.height)

        let rectangle = CGRect(x: rect.width/2 - side/2 + 10, y: 10, width: side - 20, height: side - 20)
        context.addEllipse(in: rectangle)
        context.setFillColor(UIColor.brown.cgColor)
        context.fillEllipse(in: rectangle)
    }
}
