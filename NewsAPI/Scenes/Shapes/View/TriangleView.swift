//
//  TriangleView.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 26/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import UIKit

@IBDesignable
class TriangleView: UIView {

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }

        context.setFillColor(UIColor.purple.cgColor)

        context.move(to: CGPoint(x: rect.midX, y: 10))
        context.addLine(to: CGPoint(x: rect.width - 10, y: rect.height - 10))
        context.addLine(to: CGPoint(x: 10, y: rect.height - 10))
        context.addLine(to: CGPoint(x: rect.midX, y: 10))

        context.fillPath()
    }
}
