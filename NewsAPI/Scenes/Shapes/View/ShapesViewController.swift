//
//  ShapesViewController.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 26/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import UIKit

enum AnimationType {
    case alpha
    case size
    case rotation
    case none

    var animation: CABasicAnimation? {
        switch self {
        case .alpha:
            let animation = CABasicAnimation(keyPath: "opacity")
            animation.fromValue = 1
            animation.toValue = 0
            animation.duration = 1
            animation.autoreverses = true
            return animation

        case .rotation:
            let animation = CABasicAnimation(keyPath: "transform.rotation")
            animation.fromValue = 0
            animation.toValue = Double.pi * 4
            animation.duration = 1
            return animation

        case .size:
            let animation = CABasicAnimation(keyPath: "transform.scale")
            animation.fromValue = 1
            animation.toValue = 1.5
            animation.autoreverses = true
            animation.duration = 1
            return animation

        case .none:
            return nil
        }
    }
}

class ShapesViewController: UIViewController {

    // MARK: - Properties

    @IBOutlet private weak var buttonDrag: UIButton!
    @IBOutlet private weak var starView: StarView!
    @IBOutlet private weak var triangleView: TriangleView!
    @IBOutlet private weak var circleView: CircleView!

    private let cornerSide: CGFloat = 150
    private var topLeftCorner: CGRect!
    private var topRightCorner: CGRect!
    private var bottomLeftCorner: CGRect!

    private var bottomConstraint: NSLayoutConstraint?

    lazy var menuView: MenuView? = {
        guard let menuView = Bundle.main
            .loadNibNamed("MenuView", owner: nil, options: nil)?.first as? MenuView else { return nil }

        menuView.delegate = self

        view.addSubview(menuView)

        self.bottomConstraint = menuView.bottomAnchor.constraint(
            equalTo: self.view.bottomAnchor,
            constant: menuView.frame.height)
        self.bottomConstraint?.isActive = true

        menuView.centerXAnchor.constraint(
            equalTo: self.view.centerXAnchor).isActive = true

        self.view.layoutIfNeeded()

        return menuView
    }()

    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        let gesture = UIPanGestureRecognizer(target: self, action: #selector(userDragged(gesture:)))
        buttonDrag.addGestureRecognizer(gesture)
        buttonDrag.isUserInteractionEnabled = true

        topLeftCorner = CGRect(
            x: 0,
            y: 0,
            width: cornerSide,
            height: cornerSide)
        topRightCorner = CGRect(
            x: view.frame.width - cornerSide,
            y: 0,
            width: cornerSide,
            height: cornerSide)
        bottomLeftCorner = CGRect(
            x: 0,
            y: view.frame.height - cornerSide,
            width: cornerSide,
            height: cornerSide)
    }

    // MARK: - User actions

    @objc private func userDragged(gesture: UIPanGestureRecognizer) {
        let currentCenter = buttonDrag.center

        switch gesture.state {
        case .cancelled, .failed:
            buttonDrag.center = currentCenter
        default:
            let loc = gesture.location(in: view)
            if view.bounds.contains(loc) {
                buttonDrag.center = loc
            } else {
                buttonDrag.center = currentCenter
            }
        }
    }

    @IBAction private func showMenu(sender: UIButton) {
        guard let menu = menuView else { return }

        if self.view.frame.contains(menu.frame) {
            self.bottomConstraint?.constant = menu.frame.height
        } else {
            self.bottomConstraint?.constant = -view.safeAreaInsets.bottom
        }

        UIView.animate(withDuration: 0.5) { [unowned self] in
            self.view.layoutIfNeeded()
        }
    }
}

// MARK: - private methods
private extension ShapesViewController {

    func animate(shape: MenuView.Shape) {
        let viewToAnimate: UIView
        let animationType: AnimationType

        switch shape {
        case .circle:
            viewToAnimate = circleView
        case .star:
            viewToAnimate = starView
        case .triangle:
            viewToAnimate = triangleView
        }

        if topRightCorner.intersects(buttonDrag.frame) {
            animationType = .alpha
        } else if topLeftCorner.intersects(buttonDrag.frame) {
            animationType = .rotation
        } else if bottomLeftCorner.intersects(buttonDrag.frame) {
            animationType = .size
        } else {
            animationType = .none
        }

        if animationType != .none {
            viewToAnimate.layer.add(animationType.animation!, forKey: "Animation")
        }
    }
}

// MARK: - MenuViewDelegate
extension ShapesViewController: MenuViewDelegate {

    func menuDidSelect(shape: MenuView.Shape) {
        animate(shape: shape)
    }
}
