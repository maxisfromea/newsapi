//
//  HomeViewModel.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 25/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation

protocol HomeVMCD: class {
    func homeViewModelShouldShowShapes()
    func homeViewModelShouldShow(article: Article)
}

protocol HomeVMVD: class {
    func shouldReloadView()
    func hideMoreButton()
    func show(error: String)
}

protocol HomeViewModel {

    var numberOfItems: Int { get }

    var view: HomeVMVD? { get set }
    var coordinator: HomeVMCD? { get set }

    func item(at index: Int) -> Article?
    func didSelectItem(at index: Int)
    func didPressMore()
    func didPressShapes()
    func didPressRefresh()
}

final class HomeViewModelImpl: HomeViewModel {
    typealias Dependencies = HasNetworkService
    private let dependencies: Dependencies

    private var articles = [Article]()
    private var totalResults = -1
    private var pageNumber = 1
    private var inProgress = false

    weak var coordinator: HomeVMCD?
    weak var view: HomeVMVD?

    init(dependencies: Dependencies) {
        self.dependencies = dependencies

        loadData()
    }

    var numberOfItems: Int {
        return articles.count
    }

    func item(at index: Int) -> Article? {
        guard articles.indices.contains(index) else { return nil }
        return articles[index]
    }

    func didSelectItem(at index: Int) {
        guard articles.indices.contains(index) else { return }
        coordinator?.homeViewModelShouldShow(article: articles[index])
    }

    func didPressMore() {
        loadData()
    }

    func didPressShapes() {
        coordinator?.homeViewModelShouldShowShapes()
    }

    func didPressRefresh() {
        loadData()
    }
}

// MARK: - private methods
private extension HomeViewModelImpl {

    var shouldHideMoreButton: Bool {
        return Constants.pageSize * (pageNumber-1) > totalResults && totalResults != -1
    }

    func loadData() {
        guard !shouldHideMoreButton else {
            view?.hideMoreButton()
            return
        }

        guard !inProgress else { return }
        inProgress = true

        dependencies.networkService.getNews(pageNumber: pageNumber) { [unowned self] (response) in
            switch response {
            case .error(let error):
                self.view?.show(error: error)

            case .success(let news):
                self.pageNumber += 1

                self.articles += news.articles
                self.totalResults = news.totalResults

                self.view?.shouldReloadView()
            }

            self.inProgress = false
        }
    }
}
