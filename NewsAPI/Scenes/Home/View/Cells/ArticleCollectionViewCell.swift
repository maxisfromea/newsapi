//
//  ArticleCollectionViewCell.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 25/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import UIKit
import SDWebImage

protocol ReusableElement {
    static var identifier: String { get }
}

class ArticleCollectionViewCell: UICollectionViewCell, ReusableElement {
    static let identifier = "ArticleCollectionViewCell"

    @IBOutlet private weak var labelAuthor: UILabel!
    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var labelContent: UILabel!
    @IBOutlet private weak var imageViewArticle: UIImageView!

    override func prepareForReuse() {
        super.prepareForReuse()

        labelAuthor.text = nil
        labelTitle.text = nil
        labelContent.text = nil
    }


    func setup(with article: Article) {
        labelTitle.text = article.title
        labelAuthor.text = article.author ?? "Anonymous"
        labelContent.text = article.description

        imageViewArticle.sd_setImage(
            with: article.urlToImage,
            placeholderImage: UIImage(named: "placeholder"),
            options: .forceTransition)
    }
}
