//
//  HomeViewController.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 25/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    // MARK: - User properties

    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private var menuView: MenuView!

    var viewModel: HomeViewModel!
    var isMoreButtonHidden = false

    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.view = self
        navigationController?.navigationBar.isTranslucent = false

        let shapesButton = UIBarButtonItem(
            title: "Shapes",
            style: .done,
            target: self,
            action: #selector(showShapes(sender:)))
        navigationItem.rightBarButtonItem = shapesButton

        let refreshButton = UIBarButtonItem(
            barButtonSystemItem: .refresh,
            target: self,
            action: #selector(refresh(sender:)))
        navigationItem.leftBarButtonItem = refreshButton
    }

    // MARK: - User actions

    @objc private func showShapes(sender: Any) {
        viewModel.didPressShapes()
    }

    @objc private func refresh(sender: Any) {
        viewModel.didPressRefresh()
    }
}

// MARK: - HomeViewModelViewDelegate
extension HomeViewController: HomeVMVD {

    func show(error: String) {
        DispatchQueue.main.async { [unowned self] in
            let alertController = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alertController, animated: true)
        }
    }

    func shouldReloadView() {
        DispatchQueue.main.async { [unowned self] in
            self.collectionView.reloadSections(IndexSet(integer: 0))
        }
    }

    func hideMoreButton() {
        DispatchQueue.main.async { [unowned self] in
            self.isMoreButtonHidden = true
            self.collectionView.reloadSections(IndexSet(integer: 0))
        }
    }
}

extension HomeViewController: UICollectionViewDataSource {

    func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int) -> Int {

        return viewModel.numberOfItems
    }

    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: ArticleCollectionViewCell.identifier,
            for: indexPath) as? ArticleCollectionViewCell,
            let article = viewModel.item(at: indexPath.row) else {

                return UICollectionViewCell()
        }

        cell.setup(with: article)

        return cell
    }

    func collectionView(
        _ collectionView: UICollectionView,
        viewForSupplementaryElementOfKind kind: String,
        at indexPath: IndexPath) -> UICollectionReusableView {

        switch kind {
        case UICollectionView.elementKindSectionFooter:
            guard let footer = collectionView.dequeueReusableSupplementaryView(
                ofKind: kind,
                withReuseIdentifier: CollectionViewFooter.identifier,
                for: indexPath) as? CollectionViewFooter else {

                    return UICollectionReusableView()
            }

            footer.delegate = self
            footer.isMoreHidden = isMoreButtonHidden

            return footer

        default:
            assertionFailure("Unknow type")
            return UICollectionReusableView()
        }
    }
}

extension HomeViewController: UICollectionViewDelegate {

    func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath) {

        viewModel.didSelectItem(at: indexPath.row)
    }
}

// MARK: - CollectionViewFooterDelegate
extension HomeViewController: CollectionViewFooterDelegate {

    func footerDidPressMore() {
        viewModel.didPressMore()
    }
}
