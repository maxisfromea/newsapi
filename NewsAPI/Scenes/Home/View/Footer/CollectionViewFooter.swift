//
//  CollectionViewFooter.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 25/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import UIKit

protocol CollectionViewFooterDelegate: class {
    func footerDidPressMore()
}

class CollectionViewFooter: UICollectionReusableView, ReusableElement {
    static let identifier = "CollectionViewFooter"

    @IBOutlet private weak var buttonMore: UIButton!
    @IBOutlet private weak var labelNoMoreData: UILabel!

    weak var delegate: CollectionViewFooterDelegate?

    var isMoreHidden = false {
        didSet {
            buttonMore.isHidden = isMoreHidden
            labelNoMoreData.isHidden = !buttonMore.isHidden
        }
    }

    @IBAction func didPressMore(sender: UIButton) {
        delegate?.footerDidPressMore()
    }
}
