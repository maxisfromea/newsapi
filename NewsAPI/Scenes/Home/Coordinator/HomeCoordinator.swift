//
//  HomeCoordinator.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 25/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

final class HomeCoordinator {
    typealias Dependencies = HasNetworkService
    private let dependencies: Dependencies

    private let storyboard = UIStoryboard(name: "Main", bundle: nil)

    let rootViewController: UINavigationController

    init(rootController: UINavigationController, dependencies: Dependencies) {
        self.rootViewController = rootController
        self.dependencies = dependencies
    }

    func start() {
        guard let viewController = storyboard.instantiateViewController(
            withIdentifier: "HomeViewController") as? HomeViewController else { return }

        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        viewModel.coordinator = self
        viewController.viewModel = viewModel

        rootViewController.pushViewController(viewController, animated: true)
    }
}

// MARK: - HomeViewModelCoordinatorDelegate
extension HomeCoordinator: HomeVMCD {

    func homeViewModelShouldShowShapes() {
        let viewController = storyboard.instantiateViewController(withIdentifier: "ShapesViewController")
        rootViewController.pushViewController(viewController, animated: true)
    }

    func homeViewModelShouldShow(article: Article) {
        guard let viewController = storyboard
            .instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController else { return }
        let viewModel = DetailViewModelImpl(article: article)
        viewModel.coordinator = self
        viewController.viewModel = viewModel

        rootViewController.pushViewController(viewController, animated: true)
    }
}

// MARK: - DetailViewModelCoordinatorDelegate
extension HomeCoordinator: DetailVMCD {

    func detailViewModelShouldGoBack() {
        rootViewController.popViewController(animated: true)
    }

    func detailViewModelShouldOpen(url: URL) {
        let safariVC = SFSafariViewController(url: url)
        rootViewController.present(safariVC, animated: true)
    }
}
