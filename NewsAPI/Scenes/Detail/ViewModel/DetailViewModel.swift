//
//  DetailViewModel.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 26/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation

protocol DetailVMCD: class {
    func detailViewModelShouldGoBack()
    func detailViewModelShouldOpen(url: URL)
}

protocol DetailViewModel {
    var author: String { get }
    var title: String { get }
    var content: String { get }
    var description: String { get }
    var imageURL: URL? { get }
    var pageURL: URL? { get }

    var coordinator: DetailVMCD? { get set }

    func didPressBack()
    func didPressOpenURL()
}

final class DetailViewModelImpl: DetailViewModel {
    private let article: Article

    let author: String
    let title: String
    let content: String
    let imageURL: URL?
    let pageURL: URL?
    let description: String

    weak var coordinator: DetailVMCD?

    init(article: Article) {
        self.article = article

        author = self.article.author ?? "Anonymous"
        title = self.article.title ?? "N/A"
        content = self.article.content ?? "N/A"
        imageURL = self.article.urlToImage
        pageURL = self.article.url
        description = article.description ?? "N/A"
    }

    func didPressBack() {
        coordinator?.detailViewModelShouldGoBack()
    }

    func didPressOpenURL() {
        coordinator?.detailViewModelShouldOpen(url: pageURL!)
    }
}
