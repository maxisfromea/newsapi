//
//  DetailViewController.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 26/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import UIKit
import SDWebImage

class DetailViewController: UIViewController {

    // MARK: - Properties

    @IBOutlet private weak var labelAuthor: UILabel!
    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var labelContent: UILabel!
    @IBOutlet private weak var labelDescription: UILabel!
    @IBOutlet private weak var imageViewContent: UIImageView!

    var viewModel: DetailViewModel!

    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        labelTitle.text = viewModel.title
        labelAuthor.text = viewModel.author
        labelContent.text = viewModel.content
        labelDescription.text = viewModel.description
        imageViewContent.sd_setImage(
            with: viewModel.imageURL,
            placeholderImage: UIImage(named: "placeholder"),
            options: .fromCacheOnly)

        let backButton = UIBarButtonItem(
            title: "Back",
            style: .done,
            target: self,
            action: #selector(goBack(sender:)))
        navigationItem.leftBarButtonItem = backButton

        if let url = viewModel.pageURL, UIApplication.shared.canOpenURL(url) {
            let openURLButton = UIBarButtonItem(
                title: "Open URL",
                style: .done,
                target: self,
                action: #selector(openURL(sender:)))
            navigationItem.rightBarButtonItem = openURLButton
        }
    }

    // MARK: - User actions

    @objc private func goBack(sender: Any) {
        viewModel.didPressBack()
    }

    @objc private func openURL(sender: Any) {
        viewModel.didPressOpenURL()
    }
}
