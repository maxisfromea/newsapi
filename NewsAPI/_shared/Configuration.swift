//
//  Configuration.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 25/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation

protocol NetworkConfiguration {
    var baseURL: URL { get }
    var apiKey: String { get }
}

struct ConfigurationNewsAPI: NetworkConfiguration {
    let baseURL = URL(string: "https://newsapi.org/v2/top-headlines")!
    let apiKey = Constants.apiKey
}
