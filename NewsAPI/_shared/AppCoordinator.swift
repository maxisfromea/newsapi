//
//  AppCoordinator.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 25/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation
import UIKit

final class AppCoordinator {
    private let dependencies: AppDependencies
    private let window: UIWindow
    private let rootViewController: UINavigationController

    private var homeCoordinator: HomeCoordinator?

    init(_ window: UIWindow) {
        self.window = window
        rootViewController = UINavigationController()
        dependencies = AppDependencies()
    }

    func start() {
        let coordinator = HomeCoordinator(
            rootController: rootViewController,
            dependencies: dependencies)
        homeCoordinator = coordinator
        coordinator.start()

        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
    }
}
