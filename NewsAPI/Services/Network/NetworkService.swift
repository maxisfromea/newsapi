//
//  NetworkService.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 25/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation

protocol Network {
    func getNews(pageNumber: Int, completion: @escaping (ResponseData<News>) -> Void)
}

final class NetworkService: Network {

    private let dispatcher: Dispatcher

    init(dispatcher: Dispatcher) {
        self.dispatcher = dispatcher
    }

    func getNews(pageNumber: Int, completion: @escaping (ResponseData<News>) -> Void) {
        let request = HeadlinesRequest(pageNumber: pageNumber)
        dispatcher.execute(request: request) { (response) in
            ResponseParser.parse(response: response, completion: completion)
        }
    }
}





