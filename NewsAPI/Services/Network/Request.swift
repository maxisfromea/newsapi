//
//  Request.swift
//  NewsAPI
//
//  Created by Maksim Moisja on 25/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation

public enum HTTPMethod: String {
    case get = "GET"
}

public protocol Request {
    var path: String { get }
    var method: HTTPMethod { get }
    var queryParams: [String: Any]? { get }
}


struct HeadlinesRequest: Request {
    let path: String = ""
    let method: HTTPMethod = .get
    var queryParams: [String: Any]? = [String : Any]()

    init(pageNumber: Int) {
        queryParams?["pageSize"] = Constants.pageSize
        queryParams?["page"] = pageNumber
        queryParams?["country"] = "us"
    }
}

struct News: Codable {
    let articles: [Article]
    let totalResults: Int
}

struct Article: Codable {
    let author: String?
    let title: String?
    let description: String?
    let content: String?
    let urlToImage: URL?
    let url: URL?

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        author = try? values.decode(String.self, forKey: .author)
        title = try? values.decode(String.self, forKey: .title)
        description = try? values.decode(String.self, forKey: .description)
        content = try? values.decode(String.self, forKey: .content)
        urlToImage = try? values.decode(URL.self, forKey: .urlToImage)
        url = try? values.decode(URL.self, forKey: .url)
    }

    init(author: String?, title: String?, description: String?, content: String?, urlToImage: URL?, url: URL?) {
        self.author = author
        self.title = title
        self.description = description
        self.content = content
        self.url = url
        self.urlToImage = urlToImage
    }
}
