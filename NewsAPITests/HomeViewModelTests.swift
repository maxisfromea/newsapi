//
//  HomeViewModelTests.swift
//  NewsAPITests
//
//  Created by Maksim Moisja on 26/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import XCTest
@testable import NewsAPI

private final class Coordinator: HomeVMCD {
    var didCallShowShapes = false
    var articleToShow: Article?

    func homeViewModelShouldShowShapes() {
        didCallShowShapes = true
    }

    func homeViewModelShouldShow(article: Article) {
        articleToShow = article
    }
}

private final class View: HomeVMVD {
    var didCallReloadData = false
    var didCallHideMoreButton = false
    var error: String?

    func shouldReloadView() {
        didCallReloadData = true
    }

    func hideMoreButton() {
        didCallHideMoreButton = true
    }

    func show(error: String) {
        self.error = error
    }
}

class HomeViewModelTests: XCTestCase {

    let dependencies = DependenciesMock()
    var viewModel: HomeViewModel!

    override func setUp() {
        super.setUp()
        viewModel = HomeViewModelImpl(dependencies: dependencies)
    }

    override func tearDown() {
        (dependencies.networkService as? NetworkServiceMock)?.responseData = nil
        super.tearDown()
    }

    func testNumberOfItems() {
        let responseData = ResponseData.success(News(
            articles: [Article(), Article(), Article()],
            totalResults: 10))

        (dependencies.networkService as? NetworkServiceMock)?.responseData = responseData
        let viewModel = HomeViewModelImpl(dependencies: dependencies)

        XCTAssertEqual(viewModel.numberOfItems, 3)
    }

    func testItemAtIndexReturnsCorrectArticle() {
        let responseData = ResponseData.success(News(
            articles: [Article(index: 1), Article(index: 2)],
            totalResults: 10))

        (dependencies.networkService as? NetworkServiceMock)?.responseData = responseData
        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        let article = viewModel.item(at: 1)

        XCTAssertEqual(article?.author, "author2")
    }

    func testItemAtIndexReturnsNilOnInvalidIndex() {
        let responseData = ResponseData.success(News(
            articles: [Article(), Article(), Article()],
            totalResults: 10))

        (dependencies.networkService as? NetworkServiceMock)?.responseData = responseData
        let viewModel = HomeViewModelImpl(dependencies: dependencies)

        XCTAssertNil(viewModel.item(at: -1))
        XCTAssertNil(viewModel.item(at: 100))
    }

    func testShowShapesIsCalledForCoordinator() {
        let coordinator = Coordinator()
        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        viewModel.coordinator = coordinator

        viewModel.didPressShapes()

        XCTAssertTrue(coordinator.didCallShowShapes)
    }

    func testShowArticleDetailsIsCalledForCoordinator() {
        let responseData = ResponseData.success(News(
            articles: [Article(index: 1), Article(index: 2)],
            totalResults: 10))
        (dependencies.networkService as? NetworkServiceMock)?.responseData = responseData

        let coordinator = Coordinator()
        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        viewModel.coordinator = coordinator

        viewModel.didSelectItem(at: 1)

        XCTAssertEqual(coordinator.articleToShow?.author, "author2")
    }

    func testReloadDataIsCalledForView() {
        let responseData = ResponseData.success(News(
            articles: [Article(index: 1), Article(index: 2)],
            totalResults: 40))
        (dependencies.networkService as? NetworkServiceMock)?.responseData = responseData

        let view = View()
        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        viewModel.view = view

        viewModel.didPressRefresh()

        XCTAssertTrue(view.didCallReloadData)
    }

    func testShowErrorIsCalledForView() {
        let responseData = ResponseData<News>.error("Error")
        (dependencies.networkService as? NetworkServiceMock)?.responseData = responseData

        let view = View()
        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        viewModel.view = view

        viewModel.didPressRefresh()

        XCTAssertEqual(view.error, "Error")
    }

    func testHideMoreButtonIsCalledForView() {
        let responseData = ResponseData.success(News(
            articles: [Article(index: 1), Article(index: 2)],
            totalResults: 1))
        (dependencies.networkService as? NetworkServiceMock)?.responseData = responseData

        let view = View()
        let viewModel = HomeViewModelImpl(dependencies: dependencies)
        viewModel.view = view

        viewModel.didPressRefresh()

        XCTAssertTrue(view.didCallHideMoreButton)
    }
}

private extension Article {

    init(index: Int = 0) {
        self.init(
            author: "author\(index)",
            title: "title",
            description: "description",
            content: "content",
            urlToImage: nil,
            url: nil)
    }
}
