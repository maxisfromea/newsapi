//
//  NetworkServiceMock.swift
//  NewsAPITests
//
//  Created by Maksim Moisja on 26/06/2019.
//  Copyright © 2019 Maksim Moisja. All rights reserved.
//

import Foundation
@testable import NewsAPI

final class DependenciesMock: HasNetworkService {
    let networkService: Network

    init() {
        networkService = NetworkServiceMock()
    }
}

final class NetworkServiceMock: Network {

    var responseData: ResponseData<News>?

    func getNews(pageNumber: Int, completion: @escaping (ResponseData<News>) -> Void) {
        if let response = responseData {
            completion(response)
        } else {
            completion(.error("No data"))
        }
    }
}
